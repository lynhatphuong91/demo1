﻿create table ChatLieu
(
	MaChatLieu nvarchar(50) not null,
	TenChatLieu nvarchar(50) not null,
	primary key clustered (MaChatLieu ASC)
)

create table Hang
(
	MaHang nvarchar(50) not null,
	TenHang nvarchar(50) not null,
	MaChatLieu nvarchar(50) not null,
	SoLuong float(53) not null,
	DonGiaNhap float(53) not null,
	DonGiaBan float(53) not null,
	Anh nvarchar(200) not null,
	GhiChu nvarchar(200) not null,
	primary key clustered (MaHang ASC)
)

create table NhanVien 
(
	MaNhanVien nvarchar(50) not null,
	TenNhanVien nvarchar(50) not null,
	GioiTinh nvarchar(10) not null,
	DiaChi nvarchar(50) not null,
	DienThoai nvarchar(15) not null,
	NgaySinh DateTime not null,
	primary key clustered (MaNhanVien asc)
)

create table HDBan 
(
	MaHDBan nvarchar(30) not null,
	MaNhanVien nvarchar(10) not null,
	NgayBan DateTime not null,
	MaKhach nvarchar(10) not null,
	TongTien float (53) not null,
	primary key clustered (MaHDBan asc)
)

create table ChiTietHDBan 
(
	MaHDBan nvarchar(30) not null,
	MaHang nvarchar(50) not null,
	SoLuong float (53) not null,
	DonGia float (53) not null,
	GiamGia float (53) not null,
	ThanhTien float (53) not null,
	primary key clustered (MaHDBan asc, MaHang asc)
)

create table Khach (
	MaKhach nvarchar(10) not null,
	TenKhach nvarchar(50) not null,
	DiaChi nvarchar(50) not null,
	DienThoai nvarchar(50) not null,
	primary key clustered (MaKhach asc)
)


