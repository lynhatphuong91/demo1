﻿--Thêm dữ liệu vào bảng Chất Liệu
create proc sp_ChatLieu_Insert
(
	@MaChatLieu nvarchar(50),
	@TenChatLieu nvarchar(50)
)
as
Insert into ChatLieu 
Values (@MaChatLieu,@TenChatLieu)

--Loi goi:
sp_ChatLieu_Insert N'Gobth', N'Gỗ bình thường'
--Cập nhật bảng chất liệu
create proc sp_ChatLieu_Update
(
	@MaChatLieu nvarchar(50),
	@TenChatLieu nvarchar(50)
)
as
update ChatLieu set TenChatLieu = @TenChatLieu
where MaChatLieu = @MaChatLieu
--Loi goi:
sp_ChatLieu_Update 
--Xóa một field trong bảng Chất Liệu
Create proc sp_ChatLieu_Delete
(
	@MaChatLieu nvarchar(50)
)
as
delete ChatLieu where MaChatLieu = @MaChatLieu
--Loi goi sp_ChatLieu_Delete N''

--Lấy dữ liệu từ bảng Nhân Viên
create proc sp_NhanVien_Select
as
select * 
from NhanVien

--Lấy mã nhân viên của bảng Nhân Viên
create proc sp_NhanVien_SelectMaNhanVien
(@MaNhanVien nvarchar(50))
as
select MaNhanVien 
from NhanVien
where MaNhanVien = @MaNhanVien

--Insert Nhân viên
create proc sp_NhanVien_Insert
(
	@MaNhanVien nvarchar(10),
	@TenNhanVien nvarchar(50),
	@GioiTinh nvarchar(10),
	@DiaChi nvarchar(50),
	@DienThoai nvarchar(15),
	@NgaySinh datetime
)
as
insert into NhanVien
values (@MaNhanVien,
	@TenNhanVien,
	@GioiTinh,
	@DiaChi,
	@DienThoai,
	@NgaySinh )


--sp_NhanVien_Insert N'Ma', N'Ten', N'gt', N'DiaChi', N'DienThoai', N'ngaysinh'

--Xóa nhân viên:
create proc sp_NhanVien_Update
(
	@MaNhanVien nvarchar(10),
	@TenNhanVien nvarchar(50),
	@GioiTinh nvarchar(10),
	@DiaChi nvarchar(50),
	@DienThoai nvarchar(15),
	@NgaySinh datetime
)
as
update NhanVien set 
					TenNhanVien = @TenNhanVien,
					GioiTinh = @GioiTinh,
					DiaChi = @DiaChi,
					DienThoai = @DienThoai,
					NgaySinh = @NgaySinh
Where MaNhanVien = @MaNhanVien
--Loi goi: 
sp_NhanVien_Update N'MaNhanVien'

--Xóa nhân viên ở bảng Nhân Viên
create proc sp_NhanVien_Delete
(@MaNhanVien nvarchar(10))
as
delete NhanVien
where MaNhanVien = @MaNhanVien

--Lấy dữ liệu từ bảng Khách
create proc sp_Khach_selectAll
as
select * from Khach

--Lấy mã khách hàng từ bảng Khách Hàng
create proc sp_Khach_selectMa
(
	@MaKhachHang nvarchar(10)
)
as
select MaKhach from Khach
where MaKhach = @MaKhachHang
--Loi goi:
sp_Khach_selectMa N'MaKhach'

--Khach Hang Insert
alter proc sp_Khach_Insert
(
	@MaKhach nvarchar(10),
	@TenKhach nvarchar(50),
	@DiaChi nvarchar(50),
	@DienThoai nvarchar(15)
)
as
	insert into Khach
	values (@MaKhach, @TenKhach, @DiaChi, @DienThoai)
--Loi goi:
--Update bảng Khách Hàng
create proc sp_Khach_Update
(
	@MaKhach nvarchar(10),
	@TenKhach nvarchar(50),
	@DiaChi nvarchar(50),
	@DienThoai nvarchar(15)
)
as
	update Khach set
						TenKhach = @TenKhach,
						DiaChi = @DiaChi,
						DienThoai = @DienThoai
				where MaKhach = @MaKhach
--Xóa trong bảng Khách Hàng
alter proc sp_Khach_DeleteField
(
	@MaKhach nvarchar(10)
)
as
	delete Khach where MaKhach = @MaKhach

--Lấy ra bảng Chất Liệu
create proc sp_ChatLieu_selectAll
as
	select * from ChatLieu

--Lấy thông tin từ bảng Hàng
create proc sp_Hang_selectAll
as
	select * from Hang
--Lấy tên chất liệu từ bảng chất liệu
create proc sp_ChatLieu_SelectTenChatLieu_MaChatLieu
(
	@MaChatLieu nvarchar(50)
)
as
	select TenChatLieu from ChatLieu
	where MaChatLieu = @MaChatLieu
--Lấy ảnh từ bảng Hàng
Create proc sp_Hang_SelectAnh
(
	@MaHang nvarchar(10)
)
as
	select Anh from Hang where MaHang = @MaHang
--Lấy ghi chú từ bảng Hàng
create proc sp_Hang_SelectGhiChu
(
	@MaHang nvarchar(10)
)
as
	select GhiChu from Hang where MaHang = @MaHang
--Lấy mã hàng từ bảng Hàng kiểm tra tồn tại Mã Hàng
create proc sp_Hang_MaHang_TonTaiKhong
(@MaHang nvarchar(10))
as
	select MaHang from Hang where MaHang = @MaHang
--Insert Hàng vào bảng
create proc sp_Hang_Insert
(
	@MaHang nvarchar(50),
	@TenHang nvarchar(50),
	@MaChatLieu nvarchar(50),
	@SoLuong float,
	@DonGiaNhap float,
	@DonGiaBan float,
	@Anh nvarchar(200),
	@GhiChu nvarchar(200)
)
as
	insert into Hang
	values (
		@MaHang, @TenHang, @MaChatLieu, @SoLuong, @DonGiaNhap, @DonGiaBan, @Anh, @GhiChu
	)
--Lời gọi:
sp_Hang_Insert N'MaHang', N'TenHang', N'MaChatLieu', SL, DGN, DGX, 

--Update bảng Hàng
alter proc sp_Hang_Update
(	@MaHang nvarchar(50),
	@TenHang nvarchar(50),
	@MaChatLieu nvarchar(50),
	@SoLuong float,
	@Anh nvarchar(200),
	@GhiChu nvarchar(200)
)
as
	update Hang set
		TenHang = @TenHang, MaChatLieu = @MaChatLieu, SoLuong = @SoLuong,
		Anh = @Anh, GhiChu = @GhiChu
	where MaHang = @MaHang

--Xóa giá trị trong bảng Hàng
create proc sp_Hang_DeleteField
(
	@MaHang nvarchar(50)
)
as
	delete Hang where MaHang = @MaHang
--Lấy dữ liệu cho LoadDataGridView HoaDon
 create proc sp_ChiTietHDBan_Hang_Select 
 (
	@MaHDBan nvarchar(30)
 )
 as
	select a.MaHang, b.TenHang, a.SoLuong, b.DonGiaBan, a.GiamGia, a.ThanhTien
	from ChiTietHDBan as a, Hang as b
	where a.MaHang = b.MaHang
	and a.MaHDBan = @MaHDBan

--Lấy ngày bán từ bảng HDBan với Mã hóa đơn bán
create proc sp_HDBan_LayNgayBan_BietMaHDBan
(
	@MaHDBan nvarchar(30)
)
as
	select NgayBan from HDBan where MaHDBan = @MaHDBan
--Lấy mã nhân viên từ bảng HDBan
create proc sp_HDBan_LayMaNhanVien
(
	@MaHDBan nvarchar(30)
)
as
	select MaNhanVien from HDBan where MaHDBan = @MaHDBan
--Lấy mã khách từ mã hóa đơn bán
create proc sp_HDBan_LayMaKhach
(
	@MaHDBan nvarchar(30)
)
as
	select MaKhach from HDBan where MaHDBan = @MaHDBan
--Lấy tổng tiền từ bảng HDBan 
create proc sp_HDBan_LayTongTien
(
	@MaHDBan nvarchar(30)
)
as
	select TongTien 
	from HDBan 
	where MaHDBan = @MaHDBan
--Kiểm tra MaxHD bán có trong bảng HDBan chưa
create proc sp_HDBan_KiemTra_MaHDBan
(
	@MaHDBan nvarchar(30)
)
as
	select MaHDBan 
	from HDBan
	where MaHDBan = @MaHDBan
--Insert bảng HDBan
create proc sp_HDBan_Insert
(
	@MaHDBan nvarchar(30),
	@MaNhanVien nvarchar(10),
	@NgayBan datetime,
	@MaKhach nvarchar(10),
	@TongTien float
)
as
	insert into HDBan
	values(	@MaHDBan,
			@MaNhanVien,
			@NgayBan,
			@MaKhach,
			@TongTien)